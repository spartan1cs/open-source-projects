import PropTypes from "prop-types";

import React, { Component } from "react";

export class ListContacts extends Component {
  static propTypes = {
    contacts: PropTypes.array.isRequired,
    onDeleteContact: PropTypes.func.isRequired,
  };

  state = {
    query: "",
  };

  updateQuery = (val) => {
    this.setState(() => ({
      query: val.trim(),
    }));
  };
  clearQuery = () => {
    this.updateQuery("");
  };
  render() {
    const { contacts, onDeleteContact } = this.props;
    const { query } = this.state;
    const showingContacts =
      query === ""
        ? contacts
        : contacts.filter((c) =>
            c.name.toLocaleLowerCase().includes(query.toLocaleLowerCase())
          );
    return (
      <div className="list-contacts">
        <div className="list-contacts-top">
          <input
            type="text"
            className="search-contacts"
            placeholder="Search Contact"
            value={query}
            onChange={(e) => this.updateQuery(e.target.value)}
          />
        </div>
        {showingContacts.length !== contacts.length && (
          <div className="showing-contacts">
            <span>
              Now showing
              {showingContacts.length} of {contacts.length}
            </span>
            <button onClick={() => this.clearQuery()}>ShowAll</button>
          </div>
        )}
        <ol className="contact-list">
          {showingContacts.map((contact, index) => (
            <li key={index} className="contact-list-item">
              <div
                className="contact-avatar"
                style={{ backgroundImage: `url(${contact.avatarURL})` }}
              ></div>
              <div className="contact-details">
                <p> {contact.name}</p> <p>{contact.handle}</p>
              </div>

              <button
                onClick={() => onDeleteContact(contact)}
                className="contact-remove"
              >
                Remove
              </button>
            </li>
          ))}
        </ol>
      </div>
    );
  }
}

export default ListContacts;

import React from "react";
import "./Header.css";
import { Avatar } from "@material-ui/core";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
function Header() {
  return (
    <div className="header">
      <div className="header__left">
        {/**avatar */}
        <Avatar className="header__avatar" alt="akash" src="" />
        {/**time icon */}
        <AccessTimeIcon />
      </div>
      <div className="header__search">
        {/**search icon  */}
        {/**input */}
      </div>
      <div className="header__right">
        {/**help icon */}
        {/** */}
      </div>
    </div>
  );
}

export default Header;

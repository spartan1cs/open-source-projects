const { createStore, combineReducers, applyMiddleware } = require("redux");
const axios = require("axios");
const thunk = require("redux-thunk").default;
const FETCH_USER_REQUEST = "FETCH_USER_REQUEST";
const FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS";
const FETCH_USER_FAILURE = "FETCH_USER_FAILURE";

//DISPATCH ACTION
const fetchUser = () => (dispatch) => {
  dispatch(fetchUserRequest());
  axios
    .get("http://jsonplaceholder.typicode.com/users")
    .then((result) => {
      const users = result.data.map((t) => t["id"]);
      dispatch(fetchUserSuccess(users));
    })
    .catch((err) => {
      dispatch(fetchUserFailure(err));
    });
};

//ACTION-----------------------------------------------------------
const fetchUserRequest = () => {
  return {
    type: FETCH_USER_REQUEST,
  };
};

const fetchUserSuccess = (users) => {
  return {
    type: FETCH_USER_SUCCESS,
    payload: users,
  };
};

const fetchUserFailure = (error) => {
  return {
    type: FETCH_USER_FAILURE,
    payload: error,
  };
};
//REDUCER------------------------------------------------------
const initialState = {
  loading: false,
  data: [],
  error: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_USER_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        error: "",
      };
    case FETCH_USER_FAILURE:
      return {
        loading: false,
        data: [],
        error: action.payload,
      };

    default:
      return state;
  }
};

//STORE STATE
const store = createStore(reducer, applyMiddleware(thunk));

//TESTING
store.dispatch(fetchUser());
console.log(store.getState());
store.subscribe(() => {
  console.log(store.getState());
});

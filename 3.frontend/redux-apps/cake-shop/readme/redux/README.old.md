--- Package installed---
npx webpack-cli init
npm i immer
npm i redux



ACTION----dispatch---->REDUCER -->STORE
---REDUCER OPERATION---

It takes
INPUT->> store and action performed by user
RETURNS->> updated store

here we need to take care that we dont mutaute the store.
SO we use immer package, We can also use immutable js or mori
in immer,
function reducers(store ={},action){
const updatedStore = produce(store, draftStore =>{ //updardstore = {...store}
store.isUpdated = action
})
}
OR
//state=[] array of bugs{id,decription,resolved}

let lastId = 0;

function reducer(state = [], action) {
  switch (action.type) {
    case "bugAdded":
      return [
        ...state,
        {
          id: ++lastId,
          description: action.payload.description,
          resolved: false,
        },
      ];

    case "bugRemoved":
      return state.filter((bug) => bug.id !== action.payload.id);

    default:
      return state;
  }
}

NOTE : the state is immutable and cannot change in place.
------------------------------------------------------------------------------------------------------------------------------------------------------------------
 The most important concept to understand here is that 
 the state in Redux comes from reducers.
 Let’s repeat: reducers produce the state of your application.
 
 
import { createStore } from "redux";
import rootReducer from "../reducers/index";
const store = createStore(rootReducer);
export default store;


 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 DISPATCHING ACTION- means sending JSON(signal) to store
 The second principle of Redux says the 
 only way to change the state is by 
 sending a signal to the store. 
 This signal is an action. 
 So "dispatching an action" means sending out a signal to the store
 Redux actions are nothing more than JavaScript objects
-------------------------------------------------------------------
 EXAMPLE: THIS JSON IS DISPATCHED
 {
  type: 'ADD_ARTICLE',
  payload: { title: 'React Redux Tutorial', id: 1 }
}
 
 
------------------------------------------------------------------------------------------------------------------------------------------------------------------
How does a reducer know when to generate the next state?
 The key here is the Redux store. 
 When an action is dispatched, 
 the store forwards a above JSON OBJ (the action object) to the reducer
-------------------------------------------------------------------

 EXAMPLE : THIS IS ACTION REDUCER( // src/js/reducers/index.js)

import { ADD_ARTICLE } from "../constants/action-types";

const initialState = {
  articles: []
};

function rootReducer(state = initialState, action) {
  if (action.type === ADD_ARTICLE) {
    state.articles.push(action.payload);  //Array.prototype.push is an impure function: it modifies the original array.breaks main Redux principle: immutability.
  }
  return state;
}

export default rootReducer;
------------------------------------------------------------------
USE This
import { ADD_ARTICLE } from "../constants/action-types";

const initialState = {
  articles: []
};

function rootReducer(state = initialState, action) {
  if (action.type === ADD_ARTICLE) {
    return Object.assign({}, state, {
      articles: state.articles.concat(action.payload)
    });
  }
  return state;
}

export default rootReducer;

-------------------------------------------------------------
Now the initial state is left pristine and the resulting state is just a copy of the initial state.
 Remember two key points for avoiding mutations in Redux:

use concat, slice, or the spread operator for arrays
use Object.assign or object spread of objects
-----------------------------------------------------------------
most important methods are just three:

getState for reading the current state of the application
dispatch for dispatching an action
subscribe for listening to state changes



//ACTION - - -dispatch- -  - > STORE<_-_-_-_- >REDUCER

//INITIALIZATION
const { createStore, combineReducers, applyMiddleware } = require("redux");
const reduxLogger = require("redux-logger");
const BUY_CAKE = "BUY_CAKE";
const BUY_ICECREAM = "BUY_ICECREAM";

const initialState = {
  numOfCakes: 50,
  numOfIcecream: 100,
};
const initialCakeState = {
  numOfCakes: 50,
};
const initialIcecreamState = {
  numOfIcecream: 100,
};

//CREATE LOGGER
const logger = reduxLogger.createLogger();

//BUY CAKE ACTION

function buyCake() {
  return {
    type: BUY_CAKE,
    payload: "Buy cake",
  };
}

//BUY ICECREAM ACTION
function buyIcecream() {
  return {
    type: BUY_ICECREAM,
    payload: "Buy icecream",
  };
}

//CREATING SEPARATE REDUCER

const cakeReducer = (state = initialCakeState, action) => {
  switch (action.type) {
    case BUY_CAKE:
      return {
        ...state,
        numOfCakes: state.numOfCakes - 1,
      };
    default:
      return state;
  }
};
const icecreamReducer = (state = initialIcecreamState, action) => {
  switch (action.type) {
    case BUY_ICECREAM:
      return {
        ...state,
        numOfIcecream: state.numOfIcecream - 1,
      };

    default:
      return state;
  }
};

//COMBINING REDUCER-----------------------------------------------------------------------------------

const rootReducer = combineReducers({
  cake: cakeReducer,
  icecream: icecreamReducer,
});

//CREATING STORE-------------------------------------------------------------------------------------
const store = createStore(rootReducer, applyMiddleware(logger));

//TESTING-----------------------------------------------------------------------------------------
console.log("initia state - > ", store.getState());
/*const unsubscribe = store.subscribe(() =>
  console.log("updated State", store.getState())
);*/

store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyIcecream());
store.dispatch(buyCake());
store.dispatch(buyCake());

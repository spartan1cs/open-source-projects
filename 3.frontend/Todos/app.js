const addForm = document.querySelector('.add');
const listTodo = document.querySelector('.todos');
const searchBar = document.querySelector('.search input');
const generateTemplate = todo => {
    console.log(todo)

    const htmlTemp = `
    <li class="list-group-item d-flex justify-content-between align-item-center">
    <span>${todo}</span>
    <i class="far fa-trash-alt delete"></i>
    </li>
    `;

    listTodo.innerHTML += htmlTemp;

}
addForm.addEventListener('submit', e => {
    e.preventDefault();
    console.log(e)
    const todo = addForm.add.value.trim();
    console.log(todo);

    if (todo.length) {
        generateTemplate(todo);
        addForm.reset();
    }



});

//delete code
listTodo.addEventListener('click', e => {
    console.log(e)
    if (e.target.classList.contains('delete')) {
        e.target.parentElement.remove()
    }
});

const filterTodo = term => {
    Array.from(listTodo.children)
        .filter(todo => !todo.textContent.toLowerCase().includes(term))
        .forEach(item => {
            item.classList.add('filtered');
        });
    Array.from(listTodo.children)
        .filter(todo => todo.textContent.toLowerCase().includes(term))
        .forEach(item => {
            item.classList.remove('filtered');

        });
}

//filtering and searching
searchBar.addEventListener('keyup', e => {

    const term = searchBar.value.trim().toLowerCase();
    console.log(term)
    filterTodo(term);
    //console.log(e.target.value)

});
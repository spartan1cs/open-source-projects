const { BUY_ICECREAM } = require("../actionTypes");

export const buyIcecream = () => {
  return {
    type: BUY_ICECREAM,
    payload: "Buy icecream",
  };
};

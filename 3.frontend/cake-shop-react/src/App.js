import React from "react";
import logo from "./logo.svg";
import "./App.css";
import CakeContainer from "./components/CakeContainer";
import store from "./redux/store";
import { Provider } from "react-redux";
import HooksCakeContainer from "./components/HooksCakeContainer";
import HooksIcecreamContainer from "./components/HooksIcecreamContainer";
import NewCakeContainer from "./components/NewCakeContainer";
import ItemContainer from "./components/ItemContainer";
function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <ItemContainer cake />
        <ItemContainer />

        <h2>NEW cake container</h2>
        <NewCakeContainer />

        <h2>Normal cake container</h2>

        <CakeContainer />
        <h2>Hooks icecream container</h2>
        <HooksIcecreamContainer />
        <h2>Hooks cake container</h2>
        <HooksCakeContainer />
      </div>
    </Provider>
  );
}

export default App;

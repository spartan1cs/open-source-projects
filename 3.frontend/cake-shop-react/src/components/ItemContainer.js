import React from "react";
import { buyIcecream, buyCake } from "../redux";
import { connect } from "react-redux";

function ItemContainer(props) {
  return (
    <div>
      <h2>ITEMS - {props.item} </h2>
      <button onClick={props.buyItem}>Buy</button>
    </div>
  );
}

const mapStateToProps = (state, myprops) => {
  const newItem = myprops.cake
    ? state.cake.numOfCake
    : state.icecream.numOfIcecream;

  return {
    item: newItem,
  };
};
const mapDispatchToProps = (dispatch, myprops) => {
  const item = myprops.cake
    ? () => dispatch(buyCake())
    : () => dispatch(buyIcecream());
  return {
    buyItem: item,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ItemContainer);

import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { buyCake, buyIcecream } from "../redux/index";
function HooksCakeContainer() {
  const numOfCake = useSelector((state) => state.cake.numOfCake);
  //const numOfIcream = useSelector((state) => state.numOfIcream);

  const dis = useDispatch();
  return (
    <div>
      <h2>Number of cakes - {numOfCake}</h2>
      <button onClick={() => dis(buyCake())}>Buy cake </button>
    </div>
  );
}

export default HooksCakeContainer;

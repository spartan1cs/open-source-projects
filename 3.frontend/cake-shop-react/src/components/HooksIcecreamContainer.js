import { useSelector, useDispatch } from "react-redux";
import { buyCake, buyIcecream } from "../redux/index";

import React from "react";

function HooksIcecreamContainer() {
  const numOfIcream = useSelector((state) => state.icecream.numOfIcecream);

  const dis = useDispatch();
  return (
    <div>
      <h2>Number of cakes - {numOfIcream}</h2>
      <button onClick={() => dis(buyIcecream())}>Buy cake </button>
    </div>
  );
}

export default HooksIcecreamContainer;

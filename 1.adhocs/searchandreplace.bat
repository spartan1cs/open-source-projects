@echo off
setlocal EnableDelayedExpansion
set "search=localhost"
set "replace=localhost12"

dir /b /s /a-d properties\Defaults.properties,app.properties,site.properties,application.properties > list1.txt

for /f "tokens=*" %%j in (list1.txt) do (
 for /f "delims=" %%i in ('type "%%~j" ^& break ^> "%%~j"') do (
        set "line=%%i"
        setlocal EnableDelayedExpansion
        set "line=!line:%search%=%replace%!"
        >>"%%~j" echo(!line!
        endlocal
    )
)
endlocal
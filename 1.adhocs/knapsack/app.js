const output = document.querySelector('.output');
var maxSack = 15;
const myObjList = [
    { profit: 10, weight: 2 },
    { profit: 5, weight: 3 },
    { profit: 15, weight: 5 },
    { profit: 7, weight: 7 },
    { profit: 6, weight: 1 },
    { profit: 18, weight: 4 },
    { profit: 3, weight: 1 }

];
var profit = 0;
var count = 0;
myObjList
    .map(obj => {
        return { profit: obj.profit, weight: obj.weight, ratio: obj.profit / obj.weight };
    })
    .sort((a, b) => b.ratio - a.ratio)
    .forEach(obj => {
        if (maxSack > 0 && obj.weight <= maxSack) {

            maxSack -= obj.weight;
            profit += obj.profit;
        } else if (maxSack > 0 && obj.weight > maxSack) {
            count++;
            if (count === 1) {
                profit += maxSack * obj.ratio;
            } else {
                return;
            }


        } else {

            return;
        }

    });


output.innerHTML = parseFloat(profit).toFixed(2);

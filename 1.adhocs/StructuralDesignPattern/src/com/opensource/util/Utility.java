package com.opensource.util;

import java.util.function.Consumer;

public class Utility {
	static Consumer<Object> printConsumer;

	static {
		printConsumer = System.out::println;
	}

	public static void print(Object term) {
		printConsumer.accept(term);
	}

}

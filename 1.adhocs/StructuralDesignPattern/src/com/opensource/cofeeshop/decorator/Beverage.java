
/**
 * 
 */
package com.opensource.cofeeshop.decorator;

/**
 * @author aakas
 *
 */
public abstract class Beverage {

	private String description;

	public Beverage(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public abstract double cost();

}

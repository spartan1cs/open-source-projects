package com.opensource.cofeeshop.decorator;

public class Milk extends AddOn {

	public Milk(Beverage beverage) {
		super("Milk", beverage);
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return beverage.cost() + 100;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return beverage.getDescription() + " with Milk";
	}
}

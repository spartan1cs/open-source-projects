package com.opensource.cofeeshop.decorator;

public class HouseHoldBlend extends Beverage {

	public HouseHoldBlend() {
		super("Household blend");
	}

	@Override
	public double cost() {

		return 250;
	}

}

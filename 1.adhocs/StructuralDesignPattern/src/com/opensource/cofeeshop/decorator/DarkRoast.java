package com.opensource.cofeeshop.decorator;

public class DarkRoast extends Beverage {

	public DarkRoast() {

		super("Darkroast");

	}

	@Override
	public double cost() {
		return 350;
	}
}

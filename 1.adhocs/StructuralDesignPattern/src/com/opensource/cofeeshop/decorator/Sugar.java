package com.opensource.cofeeshop.decorator;

public class Sugar extends AddOn {

	public Sugar(Beverage beverage) {
		super("Sugar", beverage);
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return beverage.getDescription() + " with Mocha";
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return beverage.cost() + 50;
	}
}

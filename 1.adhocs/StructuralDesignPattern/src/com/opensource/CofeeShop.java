package com.opensource;

import com.opensource.cofeeshop.decorator.HouseHoldBlend;
import com.opensource.cofeeshop.decorator.Milk;
import com.opensource.cofeeshop.decorator.Sugar;
import com.opensource.util.Utility;

public class CofeeShop {

	public static void main(String[] args) {

		HouseHoldBlend houseHoldBlend = new HouseHoldBlend();

		Utility.print(houseHoldBlend.getDescription() + " and cost-> " + houseHoldBlend.cost());
		Milk milk = new Milk(houseHoldBlend);

		Utility.print(milk.getDescription() + " and cost-> " + milk.cost());
		Sugar sugar = new Sugar(new Milk(new HouseHoldBlend()));
		Utility.print(sugar.getDescription() + " and cost-> " + sugar.cost());
	}

}
